﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Szamok.Resources;

namespace Szamok
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>


    public partial class MainWindow : Window
    {
        private static readonly int MAX = 5;
        private static readonly int ROUNDS = 10;
        private static readonly string START_MESSAGE = "Indítás a Start gombbal.";
        private static readonly Random rnd = new Random();

        private List<int> answers = new List<int>();
       
        public List<Basket> baskets = new List<Basket>();
        public bool Sum { get; set; } = true;
        public bool Sub { get; set; } = true;
        public bool Mul { get; set; } = true;
        public bool Div { get; set; } = true;
        public string player = "Játékos";
        public int numbers = MAX;
        public int rounds = ROUNDS;

        private Game game;

        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = this;

            CreateGame();
            
        }

        public void CreateGame()
        {
            game = new Game(numbers, rounds, player, Sum, Sub, Mul, Div);
            SetControl(game.IsInGame);

            // Deleting in case of previous col definitions
            while (grdAnswerRow.ColumnDefinitions.Count > 0)
            {
                grdAnswerRow.ColumnDefinitions.RemoveAt(0);
            }
            answers.Clear();
            baskets.Clear();

            // Making grids baskets and Points dynamically
            Grid grid = new Grid();

            for (int i = 0; i < numbers; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                grdAnswerRow.ColumnDefinitions.Add(col);

                Basket basket = new Basket();

                answers.Add(i + 1);

                for (int p = 1; p < numbers + 1; p++)
                {
                    Ellipse ellipse = Basket.PointFactory(p);
                    basket.ellipses.Add(ellipse);
                    basket.canvas.Children.Add(ellipse);
                }
                baskets.Add(basket);

                Grid.SetColumn(basket, i);

                grdAnswerRow.Children.Add(basket);
            }
        }

        public void SetMainButtons(bool enable)
        {
            btnStart.IsEnabled = enable;
            btnSettings.IsEnabled = enable;
            btnEnd.IsEnabled = enable;

            if (enable)
            {
                btnStart.Opacity = 1;
                btnSettings.Opacity = 1;
                btnEnd.Opacity = 1;
                SetControl(game.IsInGame);
            }
            else
            {
                btnStart.Opacity = 0.3;
                btnSettings.Opacity = 0.3;
                btnEnd.Opacity = 0.3;
            }

        }

        public void AnswerReceived(int number)
        {
            if (number == game.CurrentAnswer)
            {
                game.Points++;
                lbPoints.Content = game.Points;
                MessageBox.Show("Garutláok " + game.Player + "!\n\nHelyes a válasz!", ":)", 
                    MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            else
                MessageBox.Show("A válasz sajnos helytelen.\n\nA helyes válasz: " +
                    game.CurrentAnswer, ":(", MessageBoxButton.OK, MessageBoxImage.Hand);
            if (game.IsEndGame)
            {
                EndGame();
            }
            else
            {
                AskQuestion();
            }
        }

        // Private methods
        private void AskQuestion()
        {
            lbQuestion.Content = game.GetQuestion();
            lbQuestionTitle.Content = "Kattints a megfelelő kosárra.";

            // Randomize answers
            Shuffle(answers);
            for (int i = 0; i < baskets.Count; i++)
            {
                baskets[i].SetPoint(answers[i]);
            }
        }

        private void EmptyBaskets()
        {
            lbPoints.Content = "0";
            for (int i = 0; i < baskets.Count; i++)
            {
                baskets[i].SetPoint(0);
            }
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            StartGame();
            SetControl(game.IsInGame);
            AskQuestion();
        }

        private void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            Settings settingsWindow = new Settings();
            SetMainButtons(false);
            settingsWindow.Show();
        }

        private void BtnEnd_Click(object sender, RoutedEventArgs e)
        {
            EndGame();
        }

        private void StartGame()
        {
            CreateGame();
            game.Start();
            SetControl(true);
        }

        private void EndGame()
        {
            MessageBox.Show("A játék befejeződött.\n\nElért pontszám: " + game.Points);
            SetControl(false);
            lbQuestionTitle.Content = START_MESSAGE;
            lbQuestion.Content = "";
            game.EndGame();
            EmptyBaskets();
        }

        private void SetControl(bool gameStarted)
        {
            btnStart.IsEnabled = !gameStarted;
            btnSettings.IsEnabled = !gameStarted;
            btnEnd.IsEnabled = gameStarted;

            foreach (Basket b in baskets)
            {
                b.btnBasket.IsEnabled = gameStarted;
                if (gameStarted)
                    b.btnBasket.Opacity = 1;
                else
                    b.btnBasket.Opacity = 0.3;
            }

            if (gameStarted)
            {
                btnStart.Opacity = 0.3;
                btnSettings.Opacity = 0.3;
                btnEnd.Opacity = 1;
            } else
            {
                btnStart.Opacity = 1;
                btnSettings.Opacity = 1;
                btnEnd.Opacity = 0.3;
            }
            
        }

        // Static methods
        private static void Shuffle<T>(List<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                (list[n], list[k]) = (list[k], list[n]);
            }
        }
    }
}
