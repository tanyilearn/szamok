﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace Szamok
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        private MainWindow mainWindow;
        
        public Settings()
        {
            Closed += Settings_Closed;
            mainWindow = (MainWindow)Application.Current.MainWindow;
            InitializeComponent();

            tbPlayer.Text = mainWindow.player;
            cbBaskets.SelectedValue = mainWindow.numbers;
            slNoRounds.Value = mainWindow.rounds;
            cbSum.IsChecked = mainWindow.Sum;
            cbSub.IsChecked = mainWindow.Sub;
            cbMul.IsChecked = mainWindow.Mul;
            cbDiv.IsChecked = mainWindow.Div;
        }

        private void Settings_Closed(object? sender, EventArgs e)
        {
            mainWindow.SetMainButtons(true);
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!cbSum.IsChecked.GetValueOrDefault() &&
                   !cbSub.IsChecked.GetValueOrDefault() &&
                   !cbMul.IsChecked.GetValueOrDefault() &&
                   !cbDiv.IsChecked.GetValueOrDefault())
            {
                MessageBox.Show("Legalább 1 műveletet be kell jelölni",
                    "Hiba", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            mainWindow.player = tbPlayer.Text;
            mainWindow.numbers = int.Parse(cbBaskets.SelectedValue.ToString());
            mainWindow.rounds = (int)slNoRounds.Value;
            mainWindow.Sum = cbSum.IsChecked.GetValueOrDefault();
            mainWindow.Sub = cbSub.IsChecked.GetValueOrDefault();
            mainWindow.Mul = cbMul.IsChecked.GetValueOrDefault();
            mainWindow.Div = cbDiv.IsChecked.GetValueOrDefault();
            mainWindow.CreateGame();

            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private ComboBox GetCbBaskets()
        {
            return cbBaskets;
        }
    }
}
