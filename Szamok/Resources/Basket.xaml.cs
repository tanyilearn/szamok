﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Szamok.Resources
{
    /// <summary>
    /// Interaction logic for Basket.xaml
    /// </summary>
    public partial class Basket : UserControl
    {
        private static SolidColorBrush ellipseBrush = new SolidColorBrush(Colors.Blue);
        private static readonly int BasketRight = 55;
        private static readonly int BasketBottom = 52;
        private static readonly int SpacingHorizontally = 20;
        private static readonly int SpacingVertically = 13;

        private int points;

        public List<Ellipse> ellipses = new List<Ellipse>();

        public int Points { 
            get
            {
                return points;
            } 
            set
            {
                if (value > ellipses.Count)
                    points = ellipses.Count;
                else 
                    points = value;
            }
        }


        public static Ellipse PointFactory(int number)
        {
            Ellipse ellipse = new Ellipse();
            ellipse.Width = 10;
            ellipse.Height = 10;
            int ellipseX = BasketRight - number % 3 * SpacingHorizontally;
            int ellipseY = BasketBottom - (number - 1) / 3 * SpacingVertically;
            ellipse.Margin = new Thickness(ellipseX, ellipseY, 0, 0);
            ellipse.Fill = ellipseBrush;
            ellipse.Visibility = Visibility.Hidden;
            return ellipse;
        }

        public void SetPoint(int number)
        {
            Points = number;
            for(int i = 0; i < ellipses.Count;i++)
            {
                if (i < number)
                    ellipses[i].Visibility = Visibility.Visible;
                else
                    ellipses[i].Visibility = Visibility.Hidden;
            }
        }
        public Basket()
        {
            InitializeComponent();
        }

        private void BtnBasket_Click(object sender, RoutedEventArgs e)
        {
            if (Application.Current.MainWindow is MainWindow mainWindow) 
                mainWindow.AnswerReceived(Points);
        }
    }
}
