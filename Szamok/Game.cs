﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Szamok
{
    internal class Game
    {
        private static readonly Random rnd = new Random();
        private const string resultsFilePath = "Results.xml";
        private int countRounds = 0;
        private int MaxNumber { get; }
        private int MaxRounds { get; }
        public int CurrentAnswer { get; private set;  }
        public bool IsInGame { get; private set; } = false;
        public bool IsEndGame { get; private set; } = false;

        public string Player { get; set; }

        public int Points { get; set; }

        enum Operations
        {
            Addition,
            Subtraction,
            Multiplication,
            Division
        }

        List<Operations> operations = new List<Operations>();

        public Game(int number, int rounds, string player, 
            bool Sum = true, bool Sub = true, 
            bool Mul = true, bool Div = true) {
            MaxNumber = number;
            this.MaxRounds = rounds;
            this.Player = player;
            operations.Clear();
            if (Sum) operations.Add(Operations.Addition);
            if (Sub) operations.Add(Operations.Subtraction);
            if (Mul) operations.Add(Operations.Multiplication);
            if (Div) operations.Add(Operations.Division);
        }

        public void Start()
        {
            IsInGame = true;
        }

        public string GetQuestion()
        {
            countRounds++;
            if (countRounds >= MaxRounds)
                IsEndGame = true;
            CurrentAnswer = rnd.Next(MaxNumber) + 1;
            
            Operations operation = operations[rnd.Next(operations.Count)];
            int operand;
            string question = "";
            switch (operation)
            {
                case Operations.Addition:
                    operand = rnd.Next(CurrentAnswer);
                    question = GenerateQuestion(operand, CurrentAnswer - operand, '+');
                    break;
                case Operations.Subtraction:
                    operand = rnd.Next(MaxNumber);
                    question = GenerateQuestion(operand + CurrentAnswer, operand, '-');
                    break;
                case Operations.Multiplication:
                    do
                    {
                        operand = rnd.Next(CurrentAnswer) + 1;
                    } while (CurrentAnswer % operand != 0);
                    question = GenerateQuestion(CurrentAnswer / operand, operand, '*');
                    break;
                case Operations.Division:
                    operand = rnd.Next(MaxNumber) + 1;
                    question = GenerateQuestion(operand * CurrentAnswer, operand, '/');
                    break;
            }

            return question;
        }

        public void EndGame()
        {
            SaveResult();
            IsInGame = false;
            IsEndGame = false;
        }

        private void SaveResult()
        {
            Results results = new Results();
            results.Player = Player;
            results.Points = Points;
            results.DateTime = DateTime.Now;
            XmlSerializer Serializer = new XmlSerializer(typeof(Results));
            using (TextWriter Writer = new StreamWriter(resultsFilePath, true))
            {
                Serializer.Serialize(Writer, results);
            }
        }

        // Static methods
        private static string GenerateQuestion(int op1, int op2, char operand)
        {
            StringBuilder sbString = new StringBuilder("Mennyi ");
            return sbString.Append(op1).Append(' ').
                Append(operand).Append(' ').Append(op2).
                Append(" ?").ToString();
        }
    }
}
