﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Szamok
{
    [Serializable]
    public class Results
    {
        [XmlAttribute("Player")]
        public string Player { get; set; }
        [XmlAttribute("Points")]
        public int Points { get; set; }
        [XmlAttribute("DateTime")]
        public DateTime DateTime { get; set; }

        public Results() { }
    }
}
