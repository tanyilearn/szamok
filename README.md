# Számok - Kisgyerekekkel a számokat megismertető program

Beadandó feladatmegoldás Programozói környezetek tantárgyra.
<br/><br/><br/>

## Bevezetés

A feladat az [alábbi linken](https://drive.google.com/drive/folders/1sWke_HrwvVNnHSHFIkAOEsthMrVQ8EW2) található feladatkiírás alapján készült.

A megírt program tehát, kisgyermekekkel ismerteti meg a számokat.
<br/><br/><br/>

## A kezdőképernyő

<br/>

*A program kezdőképernyője:*

![Kezdőképernyő](Start.png)

A program minden indításakor 3 fő gomb jelenik meg felül. Ebből a játék elindítása előtt a `Start` és a `Beállítások` gomb az aktív. A `Vége` gomb csak a játék során lesz aktív. Szintén alapértelmezetten 5 kosár jelenik meg az ablak alján, amik a játék elindítása előtt inaktívak.
<br/><br/><br/>

## A játék elindítása
<br/>

Magát a játékot a `Start` gombra kattintva lehet elindítani.
A játék lényege az, hogy a program egy matematikai műveletet (összeadás, kivonás, szorzás vagy osztás) ad feladatként a felhasználónak. A felhasználó célja, hogy arra a kosárra kattintson, amelyikben pontosan annyi kék kör van, amennyi a művelet végeredménye. Minden helyes válaszért a játékos 1 pontot kap. A program minden válasz után felugró ablakban tájékoztatja a játékost, hogy a válasz helyes -e? Helytelen válasz esetén a helyes megoldás is megjelenik a felugró ablakban. 

<br/>

*Képernyő a játék elindítása után:*

![Játék képernyő](Gameplay.png)

A játék két esetben ér véget:
- a játékos a jobb oldali `Vége` gombra kattint, vagy
- a játékos válaszolt az összes (alapértelmezetten 10) kérdésre.

A játék végén az elért pontszám (a beállított játékos névvel a `Results.xml` fájlba kerül mentésre).
<br/><br/><br/>

## Beállítások

Abban az esetben, ha a felhasználó nincs játékban, a `Beállítások` gomb aktív.
A gombra kattintva az alábbi képernyőt láthatjuk.

<br/>

*Beállítások képernyő:*

![Beállítások](Settings.png)

A beállítások képernyőn lehetőségünk van megadni:
- a játékos nevét (ezzel a névvel kerülnek elmentésre a pontszámok)
- a kosarak számát (minimum 5, maximum 9)
- az egy játékban feltett kérdések számát (minimum 5, maximum 20)
- valamint, hogy mely matematikai alapműveletek szerepeljenek a kérdésekben (legalább 1 választása kötelező!)

A beállítások csak akkor kerülnek elmentésre, ha az ablak az OK gombbal került bezárásra. A Mégse gombra vagy az ablak jobb felső bezárás ikonjára kattintva a beállított értékeke nem kerülnek elmentésre.
<br/><br/><br/>

## Szerializálás

A feladatban kiírt szerializálást a `Results` osztály létrehozásával és a `System.Xml.Serialization` névtérben található `XmlSerializer` osztály segítségével oldottam meg.
<br/><br/><br/>

## További technikai információ

A program C# nyelven Visual Studio 2022 -ben lett elkészítve, .NET 6.0 keretrendszert és WPF osztálykönyvtárt használva. Ezért Windows operációs rendszer szükséges annak futtatásához.

Tesztelve Windows 10 rendszeren volt. (Buildszám: 19045.2486)

A fejlesztés során GIT verziókövető rendszert használtam, így a projekt publikusan is elérhető [az alábbi linken](https://gitlab.com/tanyilearn/szamok)
<br/><br/><br/>

## További fejlesztési lehetőségek

Kézenfekvő lenne a szerializált adatok felhasználása, mondjuk egy toplista megjelenítésének implementálásával. Bár a program nagysága nem teszi szükségessé, de további gyakorlat szerzés céljából az adatokat akár adatbázisban (például SQLite adatbázisban) is lehetne tárolni. Ezzel a szabadon választott témánál előírt CRUD műveletek is megvalósításra kerülhetnének.
<br/><br/><br/>

## Köszönet

Köszönöm az inspirációt és az átadott tudást Kovács Ádámnak, aki nélkül ez a project nem jött volna létre.
